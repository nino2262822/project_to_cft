from fastapi import Depends, FastAPI
from fastapi_users import FastAPIUsers, fastapi_users
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from database import User, get_async_session
from auth import auth_backend
from manager import get_user_manager
from schemas import UserCreate, UserRead
from models import salary


fastapi_users = FastAPIUsers[User, int](
    get_user_manager,
    [auth_backend],
)

app = FastAPI()


app.include_router(
    fastapi_users.get_auth_router(auth_backend),
    prefix="/auth/jwt",
    tags=["auth"],
)

app.include_router(
    fastapi_users.get_register_router(UserRead, UserCreate),
    prefix="/auth",
    tags=["auth"],
)


current_user = fastapi_users.current_user()


@app.get("/salary_auth")
async def protected_route(user: User = Depends(current_user),
                          session: AsyncSession = Depends(get_async_session)):
    query = select(salary.c.finance, salary.c.date).where(
        salary.c.user_id == user.id)
    resualt = await session.execute(query)
    for value in resualt:
        return f"Ваша зарплата {value.finance} рублей. Следующая плановая дата повышения {value.date}"


@app.get("/salary_noauth")
def protected_route():
    return f"Необходимо авторизоваться"
