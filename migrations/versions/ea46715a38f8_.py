"""empty message

Revision ID: ea46715a38f8
Revises: bb90822c6e70
Create Date: 2023-06-09 22:30:14.453279

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ea46715a38f8'
down_revision = 'bb90822c6e70'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
