from sqlalchemy import Date, MetaData, Integer, Numeric, String, ForeignKey, Table, Column, Boolean

metadata = MetaData()


user = Table(
    'user',
    metadata,
    Column('id', Integer, primary_key=True),
    Column('username', String, nullable=False),
    Column('email', String, nullable=False),
    Column('hashed_password', String, nullable=False),
    Column('is_active', Boolean, default=True, nullable=False),
    Column('is_superuser', Boolean, default=False, nullable=False),
    Column('is_verified', Boolean, default=True, nullable=False),
)

salary = Table(
    'salary_user',
    metadata,
    Column('id', Integer, primary_key=True),
    Column('finance', Numeric(11, 2), nullable=False),
    Column('date', Date),
    Column('user_id', Integer, ForeignKey(user.c.id)),
)
